var mongoClient = require("mongodb").MongoClient;
process.env.NODE_ENV = process.env.NODE_ENV || 'development'; 
const port = process.env.PORT || 3032;
    
    // Main starting point of the application
    const http = require('http');
    const config = require('./config/mongoose');
    const express = require('./config/express');
    
    // DB Setup
    // const db = mongoose(); 
    const app = express();
    function init() {
        try {
            mongoClient.connect(config.mongo.db, function (err) {
                if (err) {
                    console.log('Cannot Connect mongoDB.');
                    throw err;
                }
                else {
                    console.log('Connect mongoDB success.');
                }
            });
        } catch (e) {
            console.log('Cannot Connect mongoDB.');
        }
    }
    
    init();
    // Server Setup
    const server = http.createServer(app);
    server.listen(port);
    
    module.exports = app;
    
    console.log('Server listening on:', port);
    
    //test branch updateserver
    