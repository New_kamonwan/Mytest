var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;
mongoose.Promise = global.Promise;

var empSchema = new Schema({
    firstName: String,
    lastName: String
 });

var User = mongoose.model('User', empSchema);

module.exports = User;