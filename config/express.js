var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var compression = require('compression');
var path = require('path');

  module.exports = function () {
    // App Setup
      const app = express();
        if (process.env.NODE_ENV === 'development') {
          app.use(morgan('dev'));
        } else {
          app.use(compression());
        }
      // console.log(path.join(__dirname,'../public'));
      app.use(express.static(path.join(__dirname,'../public')));
      require('../app/routes/routes')(app);

      return app;
  };
