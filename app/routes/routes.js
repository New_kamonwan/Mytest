var path = require('path');
var express = require('express');
var appe = express();
var fs = require("fs")
const bcrypt = require('bcrypt');
routes = require('../controllers/homeController');
// var urlencodedParser = bodyParser.urlencoded({ extended: true })
var bodyParser = require('body-parser');
module.exports = function (app) {
    
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(express.static(path.join(__dirname,'../public')));
    app.get('/', function(req, res){
        res.sendFile(path.resolve('views/home.html'));
      });

    app.get('/register',function(req,res){
        res.sendFile(path.resolve('views/register.html'));
    });

    app.get('/stream',function(req,res){
        res.sendFile(path.resolve('views/stream.html'));
    });

    app.post('/registration', routes.register)
    // app.post('/login',  routes.singin);
    app.get('/home', routes.home);
    app.get('/insert', routes.insert);
    app.get('/name', routes.modelName);
    // app.get('/test',  login.wwd);
    app.get('/name', routes.modelName);
}
