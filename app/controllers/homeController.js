var bcrypt = require('bcrypt');
var path = require('path');
var express = require('express');
var bodyParser = require('body-parser')
var fs = require("fs")
var urlencodedParser = bodyParser.urlencoded({ extended: true })
var User = require('../../models/users');
var mongoose = require('mongoose');
/**
 * Module dependencies.
 */
exports.update = function (req, res) {
    res.status(400).send({
        success: true,
        message: "test"
    });
}

exports.rwd = function (req, res) {
    fs.readFile('student-3.json', (err, data) => {
        if (err) throw err;
        let student = JSON.parse(data);
        console.log(student);
    });

    console.log('This is after the read call');
}

exports.register = function (req, res,next) {
    const saltRounds = 10000
    var requestData = req.body;
    bcrypt.genSalt(saltRounds, function (err, getsalt) {
        bcrypt.hash(requestData.password, getsalt, function (err, gethash) {
            salt = getsalt
            // hash = gethash
            const json = {
                email: requestData.email,
                salt: salt,
                // hash: hash,
                iterations: saltRounds,
            }
            const jsonString = JSON.stringify(json)
            fs.writeFile('myuser.json', jsonString, 'utf8', function () {
                res.send('ok : ' + requestData.email + ', ' + requestData.password + ' Salt : ' + salt )
            });
        })
    })
}

exports.home = function(req, res, next) {
    User.find(function(err, docs) {
    if (err) return next(err);
    res.send(docs);
  });
};

exports.modelName = function(req, res) {
  res.send('my model name is ' + User.modelName);
};

exports.insert = function(req, res, next) {
    User.create({name: 'inserting ' + Date.now()}, function(err, doc) {
    if (err) return next(err);
    res.send(doc);
  });
};

exports.addName = function(req, res ) {
    
    var myData = new User(req.body);
    console.log(req.body)
    myData.save()
        .then(item => {
            res.send("item save database")
        })
        .catch( err => {
            res.status(400).send("unable to save to database");
        })
};